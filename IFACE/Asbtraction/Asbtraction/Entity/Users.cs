﻿using System.Security.Cryptography;

namespace Abstraction.Entity
{
    public class Users : IUsers
    {
        private const string salt = "S@lt";
        public long user_id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public bool ValidPassword(string pass)
        {
            bool resault = false;

            string passEncrypted = Utils.StringToByte.StringToValid(pass, salt);
            string password = Utils.StringToByte.StringToValid(pass, salt);
            resault = passEncrypted == password;
            //return true;
            return resault;
        }
    }
}
