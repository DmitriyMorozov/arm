﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IUsers
    {
        public long user_id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
    }
}
