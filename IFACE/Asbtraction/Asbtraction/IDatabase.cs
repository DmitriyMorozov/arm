﻿using System;
using System.Collections.Generic;
using System.Text;
using Abstraction.Database;

namespace Abstraction
{
    public interface IDatabase
    {
       
        public IDatabaseUsers Users { get; }

    }
}
