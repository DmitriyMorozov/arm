﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Abstraction.Utils
{
    public class StringToByte
    {
        public static string ByteToString(byte b)
        {
            string result = $"{(char)b}";
            return result;
        }
        public static string ByteArrToString(byte[] b)
        {
            string result = "";
            for (int i = 0; i < b.Length; i++)
            {
                result += ByteToString(b[i]);
            }
            return result;
        }
        public static string ByteArrToHashString(byte[] b)
        {
            string result = "";
            for (int i = 0; i < b.Length; i++)
            {
                result += b[i].ToString("X2");

            }
            return result;
        }
        public static byte[] StringToByteArr(string s)
        {
            byte[] result = new byte[s.Length];
            for (int i = 0; i < s.Length; i++)
            {
                result[i] = (byte)s[i];
            }
            return result;
        }
        public static string StringToValid(string s, string salt)
        {
            MD5 alg = MD5.Create();
            byte[] passArr = Utils.StringToByte.StringToByteArr(s + salt);
            alg.Initialize();
            byte[] data = alg.ComputeHash(passArr);
            return ByteArrToHashString(data);
        }
    }
}
