﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDatabaseReadWrite<T> : IDatabaseReadAll<T>, IDatabaseReadOne<T>,
        IDatabaseWrite<T>
    {
    }
}
