﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace SqLite.Data
{
    internal class DatabaseDDL
    {
        public const string Users = "CREATE TABLE Users (User_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Login STRING NOT NULL, Password STRING NOT NULL)";
        public static string Parse(string DDL)
        {
            string result = DDL.Replace("\n", "");
            result = result.Replace("\r", "");

            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);
            result = regex.Replace(result, " ");

            return result;
        }

        public readonly static Dictionary<string, string> Tables = new Dictionary<string, string>
        {
            {"Users", Users },
           

        };          

    }


}
