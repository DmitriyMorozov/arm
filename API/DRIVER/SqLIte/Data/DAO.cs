﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

#nullable enable
namespace SqLite.Data
{
    internal class DAO
    {
        private SqliteConnection connection;
        private Mutex connectionUseMutex;
        private static DAO? instance = null;
        public static DAO Instance
        {
            get
            {
                if (instance == null)
                {
                    throw (new Exception("setting not set"));
                }
                else
                {
                    return instance;
                }
            }
        }
        public static bool isConnection()
        {
            if (instance != null)
            {
                return true;
            }
            return false;
        }
        public static bool Connect(Abstraction.IDatabaseSetting settings)
        {
            bool result = false;
            instance = null;
            try
            {
                instance = new DAO(settings);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        private DAO(Abstraction.IDatabaseSetting settings)
        {
            connection = new SqliteConnection();
            connection.ConnectionString = $"Data Source={settings.Location}";
            connection.Open();
            connection.Close();
            connectionUseMutex = new Mutex();
        }
        public List<T> GetALL<T>(string query, List<SqliteParameter> parameters) where T : class, new()
        {
            List<T> result = new List<T>();
            try
            {
                connectionUseMutex.WaitOne();
                SqliteCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                if (parameters.Count > 0)
                {
                    command.Parameters.AddRange(parameters);
                }
                SqliteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(
                        FillFromRow<T>(ref reader)
                    );
                }
                connection.Close();
                connectionUseMutex.ReleaseMutex();
            }
            catch
            {
                throw new Exception("Failed at GetAll<T>");
            }
            return result;
        }
        public List<T> GetALL<T>(string query) where T : class, new()
        {
            List<T> result = new List<T>();
            try
            {
                connectionUseMutex.WaitOne();
                SqliteCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                SqliteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(
                        FillFromRow<T>(ref reader)
                    );
                }
                connection.Close();
                connectionUseMutex.ReleaseMutex();
            }
            catch
            {
                throw new Exception("Failed at GetAll<T>");
            }
            return result;
        }

        private T FillFromRow<T>(ref SqliteDataReader reader) where T : class, new()
        {
            T result = new T();
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            int cnt = reader.FieldCount;
            for (int i = 0; i < cnt; i++)
            {
                string fieldName = reader.GetName(i).ToLower();
                foreach (PropertyInfo property in properties)
                {
                    if (property.Name.ToLower() == fieldName)
                    {
                        try
                        {
                            if (property.PropertyType.Name == "DateTime")
                            {
                                property.SetValue(result, Convert.ToDateTime(reader.GetValue(i)));
                                break;
                            }
                            else
                            {
                                property.SetValue(result, reader.GetValue(i));
                                break;
                            }

                        }
                        catch
                        {
                            break;
                        }

                    }
                }
            }
            return result;
        }
        public object? GetSingle(string query)
        {
            object? result = null;
            try
            {
                connectionUseMutex.WaitOne();
                SqliteCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                result = command.ExecuteScalar();
                connection.Close();
                connectionUseMutex.ReleaseMutex();
            }
            catch (Exception ex)
            {
                connection.Close();
                connectionUseMutex.ReleaseMutex();
                throw ex;
            }

            return result;
        }
        public int? ExecuteNonQuery(string query)
        {
            int? result = null;
            try
            {
                connectionUseMutex.WaitOne();
                SqliteCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                result = command.ExecuteNonQuery();
                connection.Close();
                connectionUseMutex.ReleaseMutex();
            }
            catch (Exception)
            {
                result = null;
                throw new Exception("Failed at ExecuteNonQuery");
            }
            return result;
        }
    }
}
