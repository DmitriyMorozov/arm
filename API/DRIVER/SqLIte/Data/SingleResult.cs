﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqLIte.Data
{
    internal class SingleResult<T>
    {
        public T Value { get; set; }
        public SingleResult()
        {
            Value = default;
        }
    }
}
