﻿using Abstraction;
using Abstraction.Database;
using Data;
using Microsoft.Data.Sqlite;
using SqLite.Data;

using System;
using System.Collections.Generic;


#nullable enable

namespace SqLIte
{
    public class Database : IDatabase
    {
        private SqliteConnection connection;
        public IDatabaseUsers Users => new DatabaseUsers();

         
        public Database(IDatabaseSetting settings)
        {
            if (DAO.Connect(settings))
            {
                if (CheckDatabase())
                {
                    Console.WriteLine("База подключена");
                }
                else
                {
                    throw new Exception("Verifacted failed");
                }
            }
            else
            {
                throw new Exception("Wrong settings!");
            }
        }
        private bool CheckOrCreateDatabase()
        {
            bool result = false;
            try
            {
                connection.Open();
                SqliteCommand command = connection.CreateCommand();
                command.CommandText = ".schema ";
                connection.Close();
                result = true;
            }
            catch
            {
                ;
            }
            return result;
        }
        private bool CheckDatabase()
        {
            bool resault = false;
            foreach (KeyValuePair<string, string> item in DatabaseDDL.Tables)
            {
                resault = CheckDatabaseClient(item.Key);
            }
            return resault;
        }
        private bool CheckDatabaseClient(string tablename)
        {
            bool resault = false;
            Dictionary<string, string> DLL = DatabaseDDL.Tables;
            string dllname = DLL[key: tablename];
            DAO dao = DAO.Instance;
            object? exsist = dao.GetSingle($"SELECT sql FROM sqlite_master WHERE type = 'table' AND name = '{tablename}';");
            if (exsist == null)
            {
                int? created = dao.ExecuteNonQuery(dllname);
                if (created != null)
                {
                    resault = true;
                }
            }
            else
            {
                object? data = dao.GetSingle($"SELECT sql FROM sqlite_schema WHERE name = '{tablename}';");
                if (data != null)
                {
                    string ddl = DatabaseDDL.Parse(data.ToString());
                    resault = ddl == DatabaseDDL.Parse(dllname);
                }
            }
            return resault;
        }

    }
}

