﻿using Abstraction.Database;
using Data;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.PortableExecutable;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SqLite.Data
{
    internal class DatabaseReadWrite<T> : IDatabaseReadWrite<T> where T : class, new()
    {
        private string _tableName = "";
        private string _table_key = "";
        internal virtual string EntityName
        {
            get { return _tableName; }
            set { _tableName = value; }
        }
        internal virtual string EntityKey
        {
            get { return _table_key; }
            set { _table_key = value; }
        }
        public T CreateOne()
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            try
            {
                object? resault = DAO.Instance.GetSingle($"DELETE FROM {EntityName} WHERE {EntityKey} = {id} ; select changes();");
                return Convert.ToInt32(resault);
            }
            catch
            {
                return 0;
            }

        }

        public List<T> Get()
        {
            List<T> result = new List<T>();
            if (DAO.isConnection())
            {
                result = DAO.Instance.GetALL<T>(
                    $"SELECT * FROM {EntityName} ");
            }
            return result;
        }

        public T Get(int id)
        {
            T result = new T();
            if (DAO.isConnection())
            {
                List<SqliteParameter> parameters = new List<SqliteParameter>();
                parameters.Add(
                    new SqliteParameter("KeyValue", id)
                    );
                List<T> queryResult = DAO.Instance.GetALL<T>(
                    $"SELECT * FROM {EntityName} WHERE {EntityKey}=$KeyValue;",
                    parameters);
                if (queryResult.Count > 0)
                {
                    result = queryResult[0];
                }
            }
            return result;
        }

        public int Post(T value)
        {
            int result = 0;

            List<string> tabelnames = new List<string>();
            List<string> tablevalue = new List<string>();
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo property in properties)
            {
                tabelnames.Add(property.Name);
                tablevalue.Add(property.GetValue(value).ToString());
            }
            string Names = CreateStringForInsert(tabelnames);
            string Values = CreateStringForInsert(tablevalue);
            if (DAO.isConnection())
            {
                object? queryResult = DAO.Instance.GetSingle($"INSERT INTO {EntityName}({Names}) VALUES({Values}) ; select last_insert_rowid();");
                result = Convert.ToInt32(queryResult);
            }
            return result;
        }

        public int Put(int id, T value)
        {
            int result = 0;
            List<string> tablevalue = new List<string>();
            List<string> tabelnames = new List<string>();
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo property in properties)
            {
                tabelnames.Add(property.Name);
                tablevalue.Add(property.GetValue(value).ToString());
            }
            string resault = CreateStringForUpdate(tabelnames, tablevalue);
            if (DAO.isConnection())
            {
                object? queryResult = DAO.Instance.GetSingle($"Update {EntityName} Set {resault} where {EntityKey} = {id}; select last_insert_rowid();");
                result = Convert.ToInt32(queryResult);
            }
            return result;
        }
        public T Get(string login)
        {
            T result = new T();
            if (DAO.isConnection())
            {
                List<SqliteParameter> parameters = new List<SqliteParameter>();
                parameters.Add(
                    new SqliteParameter("KeyValue", login)
                    );
                List<T> queryResult = DAO.Instance.GetALL<T>(
                    $"SELECT * FROM {EntityName} WHERE Login =$KeyValue;",
                    parameters);
                if (queryResult.Count > 0)
                {
                    result = queryResult[0];
                }
            }
            return result;
        }

        private static string CreateStringForInsert(List<string> ts)
        {
            string resault = "";
            for (int i = 1; i != ts.Count; i++)
            {
                resault = resault + "'" + ts[i] + "'" + ",";
            }
            resault = resault.Remove(resault.Length - 1, 1);
            return resault;
        }
        private static string CreateStringForUpdate(List<string> tabelnames, List<string> tablevalue)
        {
            string resault = "";
            for (int i = 1; i != tablevalue.Count; i++)
            {
                resault = resault + tabelnames[i] + " = '" + tablevalue[i] + "',";
            }
            resault = resault.Remove(resault.Length - 1, 1);
            return resault;
        }
    }
}
