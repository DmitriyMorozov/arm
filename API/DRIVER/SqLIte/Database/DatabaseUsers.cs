﻿using Abstraction.Entity;
using SqLite.Data;

using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    class DatabaseUsers : Abstraction.Database.IDatabaseUsers
    {
        private DatabaseReadWrite<Users> ReadWrite = new DatabaseReadWrite<Users>() { EntityKey = "User_id", EntityName = "Users" };

        public IUsers Get(int id)
        {
            return ReadWrite.Get(id);
        }

        public IUsers GetByUserName(string userName)
        {
            return ReadWrite.Get(userName);
        }
    }
}
