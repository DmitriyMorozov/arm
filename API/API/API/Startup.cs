using API.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Repository.LoadDriver(configuration);
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<AuthDBContext>(options =>
            {
                options.UseOpenIddict();
            }
            );
            services.AddOpenIddict()
                 .AddCore(options =>
                 {
                     options.UseEntityFrameworkCore()
                         .UseDbContext<AuthDBContext>();
                 })
                .AddServer(options =>
                {
                    options.SetTokenEndpointUris("/connect/token");
                    options.AllowClientCredentialsFlow();
                    options.AllowPasswordFlow();
                    options.AddDevelopmentEncryptionCertificate()
                        .AddDevelopmentSigningCertificate();
                    options.UseAspNetCore()
                        .EnableTokenEndpointPassthrough();
                    options.UseAspNetCore().DisableTransportSecurityRequirement();
                    options.SetAccessTokenLifetime(TimeSpan.FromHours(8));
                    options.DisableAccessTokenEncryption();
                    options.AllowPasswordFlow();
                })
                .AddValidation(options => {
                    options.UseLocalServer();
                    options.UseAspNetCore();
                });


            services.AddSwaggerGen();
            services.AddHostedService<CreateClientsService>();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = OpenIddict.Validation.AspNetCore.OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme;
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UsePathBase("/app");
            }
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapDefaultControllerRoute();

            });
            app.UseSwagger();
            app.UseSwaggerUI();
        }
    }
}
