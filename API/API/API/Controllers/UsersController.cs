﻿using Abstraction.Entity;
using API.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        // GET api/<UsersController>
        [HttpGet("{login}/{password}")]
        public IUsers Get(string login, string password)
        {
            if (Repository.IsLoaded)
            {
                Users users = (Users)Repository.Instance.Users.GetByUserName(login);
                if (users.password != null)
                {
                    if (users.ValidPassword(password))
                    {
                        return users;
                    }
                }
                return null;
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public IUsers Get(int id)
        {
            if (Repository.IsLoaded)
            {
                return Repository.Instance.Users.Get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }


    }
}
