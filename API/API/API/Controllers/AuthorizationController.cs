using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using OpenIddict.Abstractions;
using OpenIddict.Server.AspNetCore;
using static OpenIddict.Abstractions.OpenIddictConstants;
using System.Security.Claims;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore;
using Abstraction.Entity;
using Microsoft.Extensions.Hosting;
using System.Security.Policy;


namespace API.Controllers
{
    public class AuthorizationController : Controller
    {
        private readonly IOpenIddictApplicationManager _applicationManager;

        public AuthorizationController(IOpenIddictApplicationManager applicationManager)
            => _applicationManager = applicationManager;

        [HttpPost("~/connect/token"), Produces("application/json")]
        public async Task<IActionResult> Exchange()
        {
            var request = HttpContext.GetOpenIddictServerRequest();

            Users user = (Users)Data.Repository.Instance.Users.GetByUserName(request.Username);
            Console.WriteLine(request.Password);
            if (user.ValidPassword(request.Password))
            {
                var identity = new ClaimsIdentity(
                    OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
                    OpenIddictConstants.Claims.Name,
                    OpenIddictConstants.Claims.Role);
                identity.AddClaim(OpenIddictConstants.Claims.Subject,
                    user.login);
                identity.AddClaim(OpenIddictConstants.Claims.Name, user.login);
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                return SignIn(principal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            }
            else
            {
                return Forbid(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            }

        }
    }

}