﻿using Microsoft.Extensions.Hosting;
using OpenIddict.Abstractions;
using static OpenIddict.Abstractions.OpenIddictConstants;
using System.Threading.Tasks;
using System.Threading;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace API.Data
{
    public class CreateClientsService : IHostedService
    {
        private const string appID = "1c";
        private const string appKey = "fff";

        private readonly IServiceProvider _serviceProvider;

        public CreateClientsService(IServiceProvider serviceProvider)
            => _serviceProvider = serviceProvider;

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using var scope = _serviceProvider.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<AuthDBContext>();
            await context.Database.EnsureCreatedAsync();

            var manager = scope.ServiceProvider.GetRequiredService<IOpenIddictApplicationManager>();


            if (await manager.FindByClientIdAsync(appID) is null)
            {
                await manager.CreateAsync(new OpenIddictApplicationDescriptor
                {

                    ClientId = appID,
                    ClientSecret = appKey,
                    Permissions =
                {
                    OpenIddictConstants.Permissions.Endpoints.Token,
                     Permissions.GrantTypes.ClientCredentials,
                    OpenIddictConstants.Permissions.GrantTypes.Password
                }
                });
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}