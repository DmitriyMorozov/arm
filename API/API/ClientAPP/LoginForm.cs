﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientAPP
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            
            textBox1.Text = "a";
            textBox2.Text = "a";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" & textBox2.Text != "")
            {
                RestAPI resta = new RestAPI();
                Result result = resta.Logining(textBox1.Text, textBox2.Text);
                if (result.Successful)
                {

                    this.DialogResult = DialogResult.OK;

                }
                else
                {
                    int status = 0;
                    int.TryParse(result.Data.ToString(), out status);
                    string message = status switch
                    {
                        0 => "Нет соединения с сервером!",
                        400 => "Не верный логин или пароль!",
                        _ => "Unknown Error!"
                    };
                    MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else
            {
                MessageBox.Show("Параметры для авторизации не заполненны!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
