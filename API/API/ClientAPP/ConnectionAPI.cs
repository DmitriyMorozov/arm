﻿using Abstraction.Entity;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientAPP
{

    public partial class RestAPI
    {
        private const string BaseUrl = "http://localhost:5000";
        private string User = null;
        private string Token = null;
        private DateTime TokenValid = DateTime.MinValue;
        RestClient client = new RestClient(BaseUrl);

        private const string appID = "MyApp";
        private const string appKey = "59b644fc-711b-44f6-a9b1-f916bfd3ef28";

        public Result Logining(string user, string password)
        {
            Result result = new Result();
            var request = new RestRequest($"http://localhost:5000/connect/token");
            request.AddParameter("Accept", "application/json", ParameterType.HttpHeader);
            request.AddParameter("Content", "application/x-www-form-urlencoded", ParameterType.HttpHeader);
            request.AddParameter("client_id", "MyApp", ParameterType.GetOrPost);
            request.AddParameter("client_secret", "59b644fc-711b-44f6-a9b1-f916bfd3ef28", ParameterType.GetOrPost);
            request.AddParameter("grant_type", "password", ParameterType.GetOrPost);
            request.AddParameter("username", user, ParameterType.GetOrPost);
            request.AddParameter("password", password, ParameterType.GetOrPost);
            var response = client.Execute(request, Method.Post);
            if (response.IsSuccessful)
            {
                AuthenticationResponse token = ParseAuthenticateResponse(response.Content);
                result.Successful = token.expires_in != int.MinValue;
                if (result.Successful)
                {
                    User = user;
                    Token = token.access_token;
                    TokenValid = DateTime.Now.AddSeconds(token.expires_in);
                }
            }
            result.Data = (int)response.StatusCode;
            return result;
        }
        private AuthenticationResponse ParseAuthenticateResponse(string data)
        {
            AuthenticationResponse result;
            try
            {
                result = JsonSerializer.Deserialize<AuthenticationResponse>(data);
            }
            catch
            {
                result = new AuthenticationResponse();
                result.expires_in = int.MinValue;
            }
            return result;
        }
        public bool IsAuthenticated
        {
            get
            {
                return (Token != null && DateTime.Now < TokenValid);
            }
        }
    }
}
