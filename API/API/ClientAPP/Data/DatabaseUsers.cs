﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientAPP.Data
{
    internal class DatabaseUsers
    {
        public static Task<bool> FuncGet(string login, string password)
        {
            bool result = false;
            RestClient client = new RestClient($"http://localhost:5000/api/Users/{login}:{password}");
            RestRequest request = new RestRequest() { Method = Method.Get };
            var response = client.Get(request);
            var resu = JsonConvert.SerializeObject(response.Content);

            if (resu.GetType() == typeof(bool))
            {
                result = true;
                MessageBox.Show("Вход выполнен!");
            }
            else
            {
                MessageBox.Show("Данные не верны!");
            }
            return Task.FromResult(result);

        }


    }
}

