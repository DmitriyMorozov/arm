﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSqLite
{
    internal class DatabaseSettings : Abstraction.IDatabaseSetting
    {

        public string Login { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public string DB { get; set; }

        public DatabaseSettings()
        {
            Location = "";
            DB = "";
            Login = "";
            Password = "";
        }
    }
}
