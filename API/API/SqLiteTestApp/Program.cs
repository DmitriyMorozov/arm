﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Abstraction;
using Abstraction.Entity;

#nullable enable
namespace TestSqLite
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("SqLite test APP!");
            IDatabaseSetting settings = new DatabaseSettings();
            settings.Location = "test.db";
            try
            {
                IDatabase database = null;
                string path = GetPath();
                if (path != null)
                {
                    byte[] data = File.ReadAllBytes(path);
                    Assembly driver = Assembly.Load(data);
                    foreach (Type type in driver.GetExportedTypes())
                    {
                        if (type.Name == "Database")
                        {
                            object[] param = new object[1];
                            param[0] = settings;
                            object? instance = Activator.CreateInstance(type, param);
                            if (instance != null)
                            {
                                database = database = (IDatabase)instance;

                            }
                        }
                    }

                }           
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

        }
        private static string GetPath()
        {
            string? result = null;
            string path = Path.GetFullPath(Path.Combine(Assembly.GetExecutingAssembly().Location, @"..\..\..\..\..\..\"));
            result = path + @"DRIVER\SqLIte\obj\Debug\netcoreapp3.1\SqLIte.dll";
            return result;
        }
    }
}
